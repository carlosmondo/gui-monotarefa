# Imagem
FROM python:3.7.3-slim

# Diretorio da aplicação
WORKDIR .

# Copia o conteúdo do atual diretório para o container /app
COPY . .

# Instala todos os pacotes necessarios especificados no requirements.txt
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Torna a porta 80 disponivel para o mundo fora do container
EXPOSE 80

# Define a variavel de ambiente
ENV NAME World

# Roda o app.py quando o container for iniciado
CMD ["python", "app.py"]
