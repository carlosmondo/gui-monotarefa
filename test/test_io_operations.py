import os
from unittest import TestCase

from src import exceptions as e
from src import read, write


class TestsIOOperations(TestCase):
    def setUp(self):
        arquivo = open("./text/arquivo_teste.txt", "w+")
        input_arquivo = "p1 cpu 3\np1 e/s 2\np1 cpu 6\np2 cpu 4\np3 cpu 3"
        arquivo.write(input_arquivo)

    def tearDown(self):
        os.remove("./text/arquivo_teste.txt")

    def test_not_exist_file(self):

        with self.assertRaises(e.FileNotFound) as context:
            read.read_file("texto/que/nao/existe.txt")

        self.assertTrue("Arquivo não encontrado" in str(context.exception))

    def test_cria_arquivo_final(self):

        write.creates_output_file(1, 1, 1, 1.0)

        self.assertTrue(os.path.isfile("./text/saida.txt"))
        os.remove("./text/saida.txt")

    def test_input_output_read(self):

        conteudo, tamanho = read.read_file("./text/arquivo_teste.txt")
        self.assertEqual(
            conteudo,
            [
                ["p1", "cpu", "3"],
                ["p1", "e/s", "2"],
                ["p1", "cpu", "6"],
                ["p2", "cpu", "4"],
                ["p3", "cpu", "3"],
            ],
        )
        self.assertEqual(tamanho, 5)
