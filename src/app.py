import glob
import sys
from os import path, remove

sys.path.append(path.join(path.dirname(__file__), ".."))

from src import exceptions as e, messages as m  # noqa: E402
from src.manager_task_input import refactor_content_in_list
from src.manager_task_output import simulate_monotask_screen
from src.read import read_file


def exclude_file():
    if path.isfile("./text/saida.txt"):
        remove("./text/saida.txt")


def main():

    path_file = glob.glob("./text/entrada.*")[0]

    try:

        content_file, length_file = read_file(path_file)
        content_file = refactor_content_in_list(content_file, length_file)
        simulate_monotask_screen(content_file)

    except (e.EmptyFile, e.ExtensionNotSupport, e.FileNotFound) as context:
        m.error(str(context))
        exclude_file()
        return

    except (e.IndexError, e.ProcessTypeError) as context:
        m.error(str(context))
        exclude_file()
        return


main()
