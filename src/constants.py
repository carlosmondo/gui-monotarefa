SUPPORT_EXT = (
    ".doc",
    ".docx",
    ".odt",
    ".pdf",
    ".rtf",
    ".text",
    ".txt",  # Unico que realmente a leitura é suportada
    ".wks",
    ".wps",
    ".wpd",
)
