from time import sleep

from src.constants import SUPPORT_EXT


""" Arquivo que contem todas as mensagens utilizadas no programa principal.
    O objetivo de isolar as mensagens em outro fonte, é a de deixar menos
    poluído o código principal, fazendo com que ele contenha somente o que é
    preciso para ler um arquivo de texto e escrever em um arquivo de texto.
"""


def reading_file():

    """ Simples função que imprime em tela algumas informações como:
        - Extensão de arquivos suportados pelo código.
        - Texto simples que indica que o arquivo está sendo lido.
    """

    print("Arquivos suportados:", *SUPPORT_EXT, sep="\n", flush=True)

    print("Lendo arquivo", end="", flush=True)
    sleep(1)
    print(".", end="", flush=True)
    sleep(1)
    print(".", end="", flush=True)
    sleep(1)
    print(".", end="\n\n", flush=True)
    sleep(1)


def monotask_simulator(
    process_id: str,
    process_type: str,
    duration_process: int,
    remaining_process_time: int,
):

    """ Mostra a tela do simulador de monotarefas, no qual apresenta a tarefa
        em execução.
        - O identificador do processo: process_id
        - O tipo do processo (E/S, CPU, ...): process_type
        - Duração total desse processo: duration_process
        - Tempo restante para finalizar a execução: remaining_process_time
    """

    print(
        f"Simulador SO Monotarefa\n"
        f"Executando o processo: {process_id}\n"
        f"Tipo do processo: {process_type}\n"
        f"Tempo estimado de execução: {duration_process}\n"
        f"Tempo restante: {remaining_process_time}\n"
    )


def error(context: str):

    """ Chama em tela mensagem informando qual o tipo de Exception que
        interrompeu a execução do programa.
    """

    print(f"Fim da execução com erro\nMotivo: {context}")
