from time import sleep

from src import messages as m
from src.write import creates_output_file


def simulate_monotask_screen(monotasks: list):
    """ Simula a leitura e execução de processos de monotarefa. """

    total_time_execution = 0
    cpu_time_execution = 0
    es_time_execution = 0
    total_avg_time_process = 0
    quantity_of_process = 0
    last_process = ""
    avg_time_process = 0

    for process_id, process in monotasks:

        remaning_time = int(process[1])
        total_time_execution += int(process[1])
        process_count_cycles(process_id, process, remaning_time)

        if str(process[0]) == "cpu":
            cpu_time_execution += int(process[1])
        if str(process[0]) == "e/s":
            es_time_execution += int(process[1])

        if last_process != process_id:

            quantity_of_process += 1
            last_process = process_id

            total_avg_time_process += avg_time_process
            avg_time_process = 0

        avg_time_process += int(process[1])

        print("-" * 124, "\n")

    total_avg_time_process /= quantity_of_process

    creates_output_file(
        total_time_execution,
        cpu_time_execution,
        es_time_execution,
        total_avg_time_process,
    )


def process_count_cycles(process_id: str, process: list, remaning_time: int):
    """ Faz a chamada em tela do processo monotarefa sendo executado. """

    end_of_task = False

    while not end_of_task:

        m.monotask_simulator(process_id, process[0], process[1], remaning_time)

        remaning_time -= 1
        end_of_task = remaning_time == 0
        sleep(1)
