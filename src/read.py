import os

from src import exceptions as e, messages as m
from src.constants import SUPPORT_EXT


def read_file(text_file: str):

    """ Lê o conteudo do arquivo caso a extensão seja suportada e o arquivo
        não esteja vazio ou com informações incorretas.

        Armazena em uma lista o conteúdo do arquivo"""

    if not os.path.exists(text_file):
        raise e.FileNotFound

    else:
        m.reading_file()

        if text_file.lower().endswith(SUPPORT_EXT):
            with open(text_file, "r") as content_text_file:
                lines_file = [
                    lines.rsplit()
                    for lines in (lines.strip() for lines in content_text_file)
                ]
                length_file = len(lines_file)
                if length_file == 0 or lines_file == "":
                    raise e.EmptyFile

                return (lines_file, length_file)
        else:
            raise e.ExtensionNotSupport(str(os.path.splitext(text_file)[1]))
