class ExtensionNotSupport(Exception):
    """ Exception para quando a extensão não for suportada """

    def __init__(self, ext):
        msg = "Extensão {} não suportada".format(ext)
        super(ExtensionNotSupport, self).__init__(msg)


class EmptyFile(Exception):
    """ Exception para quando o arquivo estiver vazio """

    def __init__(self):
        msg = "Arquivo de leitura não apresenta conteudo"
        super(EmptyFile, self).__init__(msg)


class FileNotFound(Exception):
    """ Exception para quando o arquivo não existir """

    def __init__(self):
        msg = "Arquivo não encontrado"
        super(FileNotFound, self).__init__(msg)


class IndexError(Exception):
    """ Exception para quando faltar informação no arquivo """

    def __init__(self):
        msg = (
            "Estrutura do arquivo incorreta. "
            "Não possui todas as informações necessárias."
        )
        super(IndexError, self).__init__(msg)


class ProcessTypeError(Exception):
    """ Exception para quando o processo no arquivo não for CPU ou E/S """

    def __init__(self):
        msg = (
            "Tipo de processo incorreto. "
            "Esta CPU não suporta este tipo de processo computacional."
        )
        super(ProcessTypeError, self).__init__(msg)
