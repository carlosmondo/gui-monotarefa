from src import exceptions as e


def refactor_content_in_list(content_file: list, length_file: int):

    """ Modifica a estrutura da lista, para fazer o relacionamento:
        - id_processo [tarefa, tempo]"""

    id_process = return_id_process(content_file, length_file)

    content_file = remove_duplicate_content(id_process, content_file)

    content_file = list(zip(id_process, content_file))

    for process in content_file:
        try:
            process[1][1]
            if process[1][0] not in ["cpu", "e/s"]:
                raise e.ProcessTypeError
        except IndexError:
            raise e.IndexError

    return content_file


def return_id_process(content_file: list, length_file: int):

    """" Retorna o id_processo da monotarefa """

    id_process = []

    for identify in range(0, length_file):
        id_process += content_file[identify][0].rsplit()

    return id_process


def remove_duplicate_content(id_process: list, content_file: list):

    """ Remove o contéudo duplicado deixado como lixo
        pela func return_id_task """

    for position in range(0, len(id_process)):
        content_file[position].remove(id_process[position])

    return content_file
