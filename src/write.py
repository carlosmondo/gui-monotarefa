import os


def creates_output_file(
    total_time_execution: int,
    cpu_time_execution: int,
    es_time_execution: int,
    total_avg_time_process: float,
):

    """ Cria o arquivo saida.txt com as informações sobre o tempo de execução
        dos processos """

    if os.path.isfile("./text/saida.txt"):
        os.remove("./text/saida.txt")

    with open("./text/saida.txt", "w+") as output_file:
        output_file.write(
            f"Tempo total de execucao: {total_time_execution}\n"
            f"Tempo total de execucao processos CPU: {cpu_time_execution}\n"
            f"Tempo total de execucao processos E/S: {es_time_execution}\n"
            f"Tempo de espera medio dos processos: {total_avg_time_process:.2}"
            f"\n"
        )

    print("Fim do Programa")
