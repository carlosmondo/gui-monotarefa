# Simulate monitoring of mono-tasks

Atividade desenvolvido durante o perido academico, com o objetivode desenvolver um programa que simule um sistema operacional monoprogramável.

O programa irá ler um arquivo de entrada contendo a lista de processos que deverá ser executada.

No arquivo conterá as informações de cada processo (nome, tipo e duração).

Exemplo do arquivo de entrada.txt:

| nome | tipo | duracao |
| ---- | ---- | ------- |
| p1   | cpu  | 2       |
| p1   | es   | 4       |
| p1   | cpu  | 4       |
| p2   | cpu  | 2       |

Cada unidade da duração do processamento do processo corresponderá a um ciclo do processador. E cada ciclo do processador deverá durar exatamente 1 segundo.

O simulador indica qual o processo está executando, quanto tempo resta e qual é o seu tipo.

Exemplo da tela do simulador:

```
Simulador SO Monotarefa

Executanto o processo: p1

Tipo do processo: CPU

Tempo estimado de execução: 8

Tempo restante: 8
```

Ao final da execução o simulador gera um arquivo com o relatório da execução com as seguintes informações:

Exemplo do arquivo de saida.txt:

```
Tempo total de execução: XXX

Tempo total de execução processos CPU: XXX

Tempo total de execução processos ES: XXX

Tempo de espera médio dos processos: XXX
```
